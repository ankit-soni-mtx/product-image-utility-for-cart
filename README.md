## Asset Description :
- This asset contains apex class which returns the image url of the Product present in the cart item of the cart.
- This has been used in project 7s:nVent B2B Commerce Cloud Implementation. 

## Asset Use :
- This apex utility can be used to retreive the image url while iterating over the cart items to add the image url in `CartItemModel`.
- Salesforce B2B Commerce have CC Product Media object which holds the images for the CC Product object records. And there define several   media types for the CC Products such as Alternate Image ,Alternative Image Thumbnail ,Product Image ,Product Search Image,Product Image Thumbnail
- Simply invoke the `getImageSrc()` method while iterating over cart items , also passing the imageType(mentioned above) as a parameter to this method.
- For reference , a method `getImageUrl()`  is defined in the `ProductImageUtility` apex class , you just need to pass a CC Cart record id to this method.