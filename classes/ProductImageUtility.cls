/**
 * Created By ankitSoni on 27/08/2020
 */
public with sharing class ProductImageUtility {

    final static String NAMESPACE = 'CCRZ';

    /**
     * Description : Return the image url of product that can be used in <img>
     * @param : MediaList , imageTarget 
     * @return : Product Image url
     */
    public static String getImageSrc(List <Map <String, Object >> mediaList, String imageTarget) {

        String imageSrc = '';
        System.debug('Media List size-' + mediaList.size());

        for (Integer mediaPos = 0; mediaPos <mediaList.size(); mediaPos++) {

            String mediaType = (String) mediaList[mediaPos].get('mediaType');
            if (mediaType == imageTarget) {

                String mediaSource = (String) mediaList[mediaPos].get('productMediaSource');

                if (mediaSource == 'Static Resource') {
                    imageSrc = '/resource/' + NAMESPACE + '__' +
                        (String) mediaList[mediaPos].get('staticResourceName') + '/' +
                        (String) mediaList[mediaPos].get('filePath');
                } else {
                    imageSrc = (String) mediaList[mediaPos].get('URI');
                }

            }
        }

        return imageSrc;
    }

    /**
     * Description : Method that demonstrates the above image utility method
     * @param : cartid
     */
    public static void getImageUrl(String cartId) {

        /*
            Image types of Products in B2B Commerce Cloud
            - Alternate Image
            - Alternative Image Thumbnail
            - Product Image	
            - Product Search Image
            - Product Image Thumbnail
         */

        String imageTarget = 'Product Search Image';
        String imageUrl = '';
        List<String> imageUrlList = new List<String>();

        Set <String> cartIdSet = new Set <String>{
            cartId
        };

        Map <String, Object> inputData = new Map <String, Object>{
            ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
            ccrz.ccApiCart.CART_IDLIST => cartIdSet,
            ccrz.ccApi.SIZING => new Map <String,Object>{
                ccrz.ccAPIProduct.ENTITYNAME => new Map <String,Object>{
                    ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL
                }
            }
        };

        try {
            Map <String, Object>outputData = ccrz.ccApiCart.fetch(inputData);

            if (outputData.get(ccrz.ccApiCart.CART_OBJLIST) != null) {
                List <Map <String, Object >> outputCartList = (List <Map <String, Object >> ) outputData.get(ccrz.ccApiCart.CART_OBJLIST);
                //outputCartList contains all the details of the fetched cart

                //Get All Products present in the cart
                List <Map <String, Object >> productList = (List <Map <String, Object >> ) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);

                if (outputCartList.size()>0) {
                    //collect the items in the cart
                    for (Map <String, Object>cartEntry: outputCartList) {

                        // get cart items in the cart
                        List <Map <String, Object >> cartItems = (List <Map <String, Object >> ) cartEntry.get('ECartItemsS');
                        for (Map <String, Object>item: cartItems) {

                            //Get Product data
                            for (Map <String, Object>productEntry: productList) {

                                if ((String) item.get('product') == (String) productEntry.get('sfid')) {

                                    // Get the media list
                                    List <Map <String, Object >> mediaList = (List <Map <String, Object >> ) productEntry.get('EProductMediasS');
                                    imageUrl = x7S_ProductImageUtility.getImageSrc(mediaList, imageTarget);
                                    imageUrlList.add(imageUrl);

                                    System.debug('Product Id: ' + productEntry.get('sfid') + ' Product Name:' + productEntry.get('sfdcName'));
                                    System.debug('Product Image URL: ' + imageUrl);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            System.debug('fetch cart Exception: ' + ex.getMessage());
        }
        
        System.debug('Image URL List: ' + imageUrlList);

    }

}